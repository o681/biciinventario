﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Table("Inventario")]
    public class Inventario
    {
        [Key]
        public int InventarioID { get; set; }
        [ForeignKey("Material")]
        public int MaterialID{ get; set; }
        public Material Material { get; set; }
        [Required]
        public int Cantidad { get; set; }
        public bool Activo { get; set; }
        public int UsuarioAuditID { get; set; }
        public DateTime FechaInsercion { get; set; }
    }
}
