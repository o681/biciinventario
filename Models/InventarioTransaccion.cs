﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Table("InventarioTransaccion")]
    public class InventarioTransaccion
    {
        [Key]
        [Required]
        public int InventarioTransaccionID { get; set; }
        [ForeignKey("Inventario")]
        public int InventarioID { get; set; }
        public Inventario Inventario { get; set; }
        public int Cantidad { get; set; }
        [ForeignKey("TipoMovimiento")]
        public int TipoMovimientoID { get; set; }
        public TipoMovimiento TipoMovimiento { get; set; }
        public int UsuarioAuditID { get; set; }
        public string Motivo{ get; set; }
        public DateTime FechaInsercion { get; set; }
    }
}
