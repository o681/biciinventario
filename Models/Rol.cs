﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Table("Rol")]
    public class Rol
    {
        [Key]
        [Required]
        public int RolId { get; set; }
        [Required]
        public string Nombre{ get; set; }
    }
}
