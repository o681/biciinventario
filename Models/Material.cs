﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
	[Table("Material")]
	public class Material
    {
		[Key]
		public int MaterialID { get; set; }
		[Required]
		public string Codigo { get; set; }
		[Required]
		public string Nombre { get; set; }
		[Required]
		public bool Activo { get; set; }
		public int UsuarioAuditID { get; set; }
		public DateTime FechaInsercion { get; set; }
	}
}
