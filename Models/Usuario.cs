﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Table("Usuario")]
    public class Usuario
    {
        [Key]
        [Required]
        public int UsuarioID { get; set; }
        [Required]
        public string Nombres { get; set; }
        [Required]
        public string Apellidos { get; set; }
        public string Telefono{ get; set; }
        [Required]
        public int RolId { get; set; }
        [Required]
        public string Correo { get; set; }
        [Required]
        public string Llave { get; set; }
        [Required]
        public string Hash { get; set; }
        [Required]
        public int UsuarioAuditID { get; set; }
        [Required]
        public DateTime FechaInsercion{ get; set; }
        [Required]
        public bool Activo{ get; set; }

    }
}
