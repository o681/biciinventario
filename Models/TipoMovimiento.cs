﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Table("TipoMovimiento")]
    public class TipoMovimiento
    {
        [Key]
        [Required]
        public int TipoMovimientoID { get; set; }
        [Required]
        public string Nombre{ get; set; }
    }
}
