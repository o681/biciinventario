﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class InventarioTransaccionDTO
    {
        public string NombreMaterial { get; set; }
        public string TipoMovimiento { get; set; }
        public string Usuario { get; set; }
        public string Motivo { get; set; }
        public int Cantidad{ get; set; }
        public DateTime FechaInsercion { get; set; }
    }
}
