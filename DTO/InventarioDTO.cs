﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class InventarioDTO
    {
        public int InventarioID { get; set; }
        public string NombreMaterial { get; set; }
        public int Cantidad { get; set; }
        public int MaterialID { get; set; }
    }
}
