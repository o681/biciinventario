﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class MovimientoDTO
    {
        public int InventarioID { get; set; }
        public int TipoMovimientoID{ get; set; }
        public int CantidadMovimiento { get; set; }
        public string Motivo{ get; set; }
    }
}
