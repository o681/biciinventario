﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public  class TipoMovimientoDTO
    {

        public int TipoMovimientoID { get; set; }
        public string Nombre { get; set; }
    }
}
