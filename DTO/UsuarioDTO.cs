﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public  class UsuarioDTO
    {
        public int UsuarioID { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Telefono { get; set; }
        public int RolId { get; set; }
        public string Correo { get; set; }
        public bool Activo{ get; set; }
        public string NombreRol{ get; set; }
        public string Password{ get; set; }
        public string ValidaPassword { get; set; }
    }
}
