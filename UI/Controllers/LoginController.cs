﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;

namespace UI.Controllers
{
    public class LoginController : BaseController
    {
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult LoginUser(LoginDTO data)
        {
            if (data == null) return Json(new Response() { Code = 500, Message = "Bad Request" });

            if (string.IsNullOrEmpty(data.Password))
                return Json(new Response() { Code = 500, Message = "Contraseña incorrecta." });
            if (data.Password.Length <= 8)
                return Json(new Response() { Code = 500, Message = "Contraseña incorrecta, debe tener mas de 8 caracteres" });

            if (string.IsNullOrEmpty(data.Correo))
                return Json(new Response() { Code = 500, Message = "Correo no válido." });

            if (!UtilidadesBLL.EsCorreoValido(data.Correo))
                return Json(new Response() { Code = 500, Message = "Correo no válido." });

            Response loginResponse = new UsuarioBLL().Login(data);
            if (loginResponse.Code == 200)
            {
                Session["user"] = loginResponse.Items;
            }
            return Json(loginResponse);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult RegistroUser(RegistroDTO data)
        {
            if (data == null) return Json(new Response() { Code = 500, Message = "Bad Request" });
            
            if(string.IsNullOrEmpty(data.Password))
                return Json(new Response() { Code = 500, Message = "Contraseña incorrecta." });
            if (data.Password.Length <= 8)
                return Json(new Response() { Code = 500, Message = "Contraseña incorrecta, debe tener mas de 8 caracteres" });
            if(data.Password != data.ValidaPassword)
                return Json(new Response() { Code = 500, Message = "Las contraseñas no coinciden." });

            if (string.IsNullOrEmpty(data.Correo))
                return Json(new Response() { Code = 500, Message = "Correo no válido." });

            if (!UtilidadesBLL.EsCorreoValido(data.Correo))
                return Json(new Response() { Code = 500, Message = "Correo no válido." });

            Response loginResponse = new UsuarioBLL().Registrar(data);

            if (loginResponse.Code == 200)
            {
                Session["user"] = loginResponse.Items;
            }
            return Json(loginResponse);
        }

        public ActionResult NoAcceso(){
            return View();
        }

        public ActionResult Perfil(){

            if (Seguridad() == null)
                return RedirectToAction("Login");
            UsuarioDTO user = Seguridad();

            return View(new UsuarioBLL().SelectById(user.UsuarioID));
        }


        public ActionResult LogOut(){
            Session["user"] = null;
            return RedirectToAction("Login");
        }

    }
}