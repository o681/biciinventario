﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;

namespace UI.Controllers
{
    public class UsuarioController : BaseController
    {
        // GET: Usuario
        public ActionResult Index()
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            UsuarioDTO user = Seguridad();
            if(user.RolId != 1)
                return RedirectToAction("NoAcceso","Login");

            return View(new UsuarioBLL().Todos());
        }

        public ActionResult AddUsuario()
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            UsuarioDTO user = Seguridad();
            if (user.RolId != 1)
                return RedirectToAction("NoAcceso", "Login");

            ViewBag.Roles = new CatalogoBLL().CatalogoRoles();
            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Activacion(UsuarioDTO data){
            if (Seguridad() == null)
                return Json(new Response() { Code = 400, Message = "Su sesión expiro, por favor ingrese de nuevo" });

            UsuarioDTO user = Seguridad();

            return Json(new UsuarioBLL().ActivaDesactiva(data, user.UsuarioID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EliminarUsuario(UsuarioDTO data)
        {
            if (Seguridad() == null)
                return Json(new Response() { Code = 400, Message = "Su sesión expiro, por favor ingrese de nuevo" });

            if(data == null)
                return Json(new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Bad request" });

            UsuarioDTO user = Seguridad();

            return Json(new UsuarioBLL().Eliminar(data, user.UsuarioID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AdicionarUsuario(UsuarioDTO data)
        {
            if (Seguridad() == null)
                return Json(new Response() { Code = 400, Message = "Su sesión expiro, por favor ingrese de nuevo" });

            if (data == null)
                return Json(new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Bad request" });

            if (string.IsNullOrEmpty(data.Password))
                return Json(new Response() { Code = 500, Message = "Contraseña incorrecta." });
            if (data.Password.Length <= 8)
                return Json(new Response() { Code = 500, Message = "Contraseña incorrecta, debe tener mas de 8 caracteres" });
            if (data.Password != data.ValidaPassword)
                return Json(new Response() { Code = 500, Message = "Las contraseñas no coinciden." });

            if(string.IsNullOrEmpty(data.Correo))
                return Json(new Response() { Code = 500, Message = "Correo no válido." });

            if (!UtilidadesBLL.EsCorreoValido(data.Correo))
                return Json(new Response() { Code = 500, Message = "Correo no válido." });


            UsuarioDTO user = Seguridad();

            return Json(new UsuarioBLL().Adicionar(data, user.UsuarioID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditarUsuario(UsuarioDTO data)
        {
            if (Seguridad() == null)
                return Json(new Response() { Code = 400, Message = "Su sesión expiro, por favor ingrese de nuevo" });

            if (data == null)
                return Json(new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Bad request" });

            if (data.UsuarioID == 0)
                return Json(new Response() { Code = 500, Message = "Usuario no válido." });

            if (string.IsNullOrEmpty(data.Correo))
                return Json(new Response() { Code = 500, Message = "Correo no válido." });


            UsuarioDTO user = Seguridad();

            return Json(new UsuarioBLL().Editar(data, user.UsuarioID));
        }
    }
}