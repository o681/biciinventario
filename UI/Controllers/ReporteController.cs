﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using DTO;

namespace UI.Controllers
{
    public class ReporteController : BaseController
    {
        // GET: Reporte
        public ActionResult Index()
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            UsuarioDTO user = Seguridad();
            if (user.RolId != 1)
                return RedirectToAction("NoAcceso", "Login");

            return View(new InventarioBLL().TodoHistorial());
        }

        public ActionResult Dashboard()
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            return View();
        }
    }
}