﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using DTO;
namespace UI.Controllers
{
    public class MaterialController : BaseController
    {
        // GET: Material
        public ActionResult Index()
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            UsuarioDTO user = Seguridad();
            if (user.RolId != 1)
                return RedirectToAction("NoAcceso", "Login");

            return View(new MaterialBLL().Todos());
        }

        public ActionResult AddMaterial()
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            return View();
        }

        public ActionResult EditMaterial(int id)
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            return View(new MaterialBLL().SelectById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult RegistrarMaterial(MaterialDTO data){
            if (Seguridad() == null)
                return Json(new Response() { Code = 400, Message = "Su sesión expiro, por favor ingrese de nuevo" });

            if(data == null)
                return Json(new Response() { Code = 500, Message = "Bad request" });

            if(string.IsNullOrEmpty(data.Codigo))
                return Json(new Response() { Code = 500, Message = "Bad request" });

            if (string.IsNullOrEmpty(data.Nombre))
                return Json(new Response() { Code = 500, Message = "Bad request" });

            UsuarioDTO user = Seguridad();

            return Json(new MaterialBLL().Registrar(data, user.UsuarioID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EliminarMaterial(MaterialDTO data)
        {
            if (Seguridad() == null)
                return Json(new Response() { Code = 400, Message = "Su sesión expiro, por favor ingrese de nuevo" });

            if (data == null)
                return Json(new Response() { Code = 500, Message = "Bad request" });

            if (data.MaterialID == 0)
                return Json(new Response() { Code = 500, Message = "Bad request" });

            UsuarioDTO user = Seguridad();

            return Json(new MaterialBLL().Eliminar(data, user.UsuarioID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditarMaterial(MaterialDTO data)
        {
            if (Seguridad() == null)
                return Json(new Response() { Code = 400, Message = "Su sesión expiro, por favor ingrese de nuevo" });

            if (data.MaterialID == 0)
                return Json(new Response() { Code = 500, Message = "Bad request" });

            if(data == null)
                return Json(new Response() { Code = 500, Message = "Bad request" });

            if (string.IsNullOrEmpty(data.Codigo))
                return Json(new Response() { Code = 500, Message = "Bad request" });

            if (string.IsNullOrEmpty(data.Nombre))
                return Json(new Response() { Code = 500, Message = "Bad request" });

            UsuarioDTO user = Seguridad();

            return Json(new MaterialBLL().Editar(data, user.UsuarioID));
        }
    }
}