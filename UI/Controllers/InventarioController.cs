﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using DTO;

namespace UI.Controllers
{
    public class InventarioController : BaseController
    {
        // GET: Inventario
        public ActionResult Index()
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            return View(new InventarioBLL().TodoInventario());
        }

        public ActionResult Movimiento(int id)
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            ViewBag.Tipos = new CatalogoBLL().CatalogoTipos();
            return View(new InventarioBLL().InventarioByID(id));
        }

        public ActionResult Historial(int id)
        {
            if (Seguridad() == null)
                return RedirectToAction("Login", "Login");

            ViewBag.Tipos = new CatalogoBLL().CatalogoTipos();
            return View(new InventarioBLL().SelectHistorial(id));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult RealizarMovimiento(MovimientoDTO data)
        {
            if (Seguridad() == null)
                return Json(new Response() { Code = 400, Message = "Su sesión expiro, por favor ingrese de nuevo" });
            
            if(data == null )
                return Json(new Response() { Code = 500, Message = "Bad request" });

            if (data.CantidadMovimiento == 0)
                return Json(new Response() { Code = 500, Message = "Cantidad no válida." });

            if (data.InventarioID == 0)
                return Json(new Response() { Code = 500, Message = "Inventario no valido." });

            UsuarioDTO user = Seguridad();

            return Json(new InventarioBLL().RealizarMovimiento(data,user.UsuarioID));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SeleccionarTodoInventario()
        {
            if (Seguridad() == null)
                return Json(new Response() { Code = 400, Message = "Su sesión expiro, por favor ingrese de nuevo" });
          
            UsuarioDTO user = Seguridad();

            return Json(new InventarioBLL().TodoInventario());
        }
    }
}