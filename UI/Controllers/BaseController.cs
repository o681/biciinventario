﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTO;
namespace UI.Controllers
{
    public class BaseController : Controller
    {
        protected UsuarioDTO Seguridad()
        {
            if (Session["user"] == null)
            {
                return null;
            }
            UsuarioDTO user = (UsuarioDTO)Session["user"];
            if (user.UsuarioID == 0)
                return null;

            return user;
        }
    }
}