﻿create Database BiciInventario
go
use BiciInventario
go


CREATE TABLE Rol
(
	RolId int not null identity(1,1) primary key,
	Nombre varchar(100)
)
GO

INSERT INTO Rol(Nombre)
VALUES('ADMINISTRADOR')
GO

INSERT INTO Rol(Nombre)
VALUES('EMPLEADO')
GO

CREATE TABLE Usuario
(
	UsuarioID int not null identity(1,1) primary key,
	Nombres varchar(100),
	Apellidos varchar(100),
	Telefono varchar(15),
	RolId int,
	Correo varchar(100),
	Llave varchar(200),
	Hash varchar(2000),
	Activo BIT NOT NULL DEFAULT(0),
	UsuarioAuditID int not null default(0),
	FechaInsercion datetime not null default(getdate())
)
GO

CREATE TABLE Material
(
	MaterialID int not null identity(1,1) primary key,
	Codigo VARCHAR(13),
	Nombre VARCHAR(254),
	Activo BIT NOT NULL DEFAULT(0),
	UsuarioAuditID int not null default(0),
	FechaInsercion datetime not null default(getdate())
)
GO

CREATE TABLE Inventario
(
	InventarioID int not null identity(1,1) primary key,
	MaterialID INT NOT NULL,
	Cantidad INT,
	Activo BIT NOT NULL DEFAULT(0),
	UsuarioAuditID int not null default(0),
	FechaInsercion datetime not null default(getdate())
)
GO

ALTER TABLE Inventario
ADD CONSTRAINT FK_Inventario_Material
FOREIGN KEY(MaterialID)
REFERENCES Material(MaterialID)
GO


CREATE TABLE TipoMovimiento
(
	TipoMovimientoID int not null identity(1,1) primary key,
	Nombre VARCHAR(20) NOT NULL,
)
GO

INSERT INTO TipoMovimiento(Nombre)
VALUES('INGRESO')
GO
INSERT INTO TipoMovimiento(Nombre)
VALUES('SALIDA')
GO



CREATE TABLE InventarioTransaccion
(
	InventarioTransaccionID int not null identity(1,1) primary key,
	InventarioID INT,
	Cantidad INT,
	TipoMovimientoID INT,
	Motivo VARCHAR(2000),
	UsuarioAuditID int not null default(0),
	FechaInsercion datetime not null default(getdate())
)
GO

ALTER TABLE InventarioTransaccion
ADD CONSTRAINT FK_InventarioTransaccion_Inventario
FOREIGN KEY(InventarioID)
REFERENCES Inventario(InventarioID)
GO

ALTER TABLE InventarioTransaccion
ADD CONSTRAINT FK_InventarioTransaccion_TipoMovimiento
FOREIGN KEY(TipoMovimientoID)
REFERENCES TipoMovimiento(TipoMovimientoID)
GO


INSERT INTO Usuario(Nombres,Apellidos,Telefono,RolId,Correo,Llave,Hash,Activo)
VALUES('Administrador','Usuario',NULL,1,'administrador@gmail.com','08R617OR3F','hr3kJZ669FH7wGRIT28KNRVb7CHH7FOfbBV+bNOheCw=',1)