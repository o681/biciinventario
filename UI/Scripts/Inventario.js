﻿
var loaderSection = "<h3 class='maller lighter grey'><i class='ace-icon fa fa-spinner fa-spin blue bigger-125'></i><small>   Un momento por favor...</small> </h3>";
var dangerSection = '<div class="alert alert-danger"><strong><i class="ace-icon fa fa-times"></i> Lo sentimos </strong> No encontramos la información.<br></div>';

var alertinfo = '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>{2} <strong>{0}</strong> {1}<br></div>';
var warninginfo = '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>{2} <strong> {0}</strong>{1}<br></div>';
var dangerinfo = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>{2} <strong><i class="ace-icon fa fa-times"></i>{0}</strong>{1}.<br></div>';
var successinfo = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>{2} <strong><i class="ace-icon fa fa-check"></i>{0}</strong>{1}.<br></div>';
var spinner = '<i class="ace-icon fa fa-spinner fa-spin orange bigger-125"></i>';

function ValidateForm(form) {

    var isOk = true;
    var formulario = '#' + form;
    var controls = $(formulario + ' textarea, ' + formulario + ' input, ' + formulario + ' select').filter('[required]:visible');

    $(controls).each(function (index) {
        if ($(this).val() == "" || $(this).val() == "0") {
            $(this).css({ 'background-color': '#FFB7B7' });
            $(this).css("border", "red solid 1px");

            //Buscamos el label en el formulario, y mostramos el mensaje
            $("label[for='" + $(this).attr('id') + "']").text($(this).attr('data-errormessage'));
            $("label[for='" + $(this).attr('id') + "']").css({ 'color': 'red' });
            isOk = false;
        }
        else {
            $(this).removeAttr('style');
            $("label[for='" + $(this).attr('id') + "']").removeAttr('style');
        }
    });
    return isOk;
}



///Muestra del modal de la aplicación, la idea es que cargue vistas parciales
function openModal(showLoader, url, title, isShow, saveButtonfunction, savebuttontext, arrayparameters) {
    $('#conciliacionmodaltitle').html(title);
    if (isShow) {
        $('#conciliacionmodal').modal()
        if (savebuttontext != null) {
            document.getElementById("btnsaveconciliacionmodal").innerText = savebuttontext;
        }
    }
    if (showLoader) {
        $('#conciliacionmodalpanel').html('<h3 class="header smaller lighter grey"><i class="ace-icon fa fa-spinner fa-spin orange bigger-125"></i>Un momento por favor,<small> estamos cargando la información</small></h3>');
    }
    if (saveButtonfunction == null) {
        $('#btnsaveconciliacionmodal').hide();
    }
    else {
        $('#btnsaveconciliacionmodal').show();
        document.getElementById("btnsaveconciliacionmodal").onclick = saveButtonfunction;
    }

    if (arrayparameters != null) {
        $('#conciliacionmodalpanel').load(url, arrayparameters, function () {
            $('#conciliacionmodaltitle').html(title);
        });
    }
    else {
        $('#conciliacionmodalpanel').load(url, function () {
            $('#conciliacionmodaltitle').html(title);
        });
    }
}

///Recarga la vista de la modal
function ReloadModal(url, title, arrayparameters) {
    if (arrayparameters != null) {
        $('#conciliacionmodalpanel').load(url, arrayparameters, function () {
            if (title != null) {
                $('#conciliacionmodaltitle').html(title);
            }
        });
    }
    else {
        $('#conciliacionmodalpanel').load(url, function () {
            if (title != null) {
                $('#conciliacionmodaltitle').html(title);
            }
        });
    }
}

function CloseModal() {
    if ($('#conciliacionmodal').hasClass('in')) {
        $('#conciliacionmodal').modal('toggle');
    }
}

function CallAjax(url, data, type, successfunction, errorfunction) {
    $.ajax
        ({
            url: url,
            data: data,
            type: type,
            success: successfunction,
            error: errorfunction
        });
}

function CloseModalComentario() {
    if ($('#modalcomentario').hasClass('in')) {
        $('#modalcomentario').modal('toggle');
    }
}


function showModalComentario(saveButtonfunction) {
    $('#btncomentarioaccion').button('reset');
    $('#textareacomentario').val("");
    document.getElementById("btncomentarioaccion").onclick = saveButtonfunction;
    $('#modalcomentario').modal();
}


function LoadButton(control) {
    $(control).addClass('running ld ld-heartbeat');
    $(control).children("span").text($(control).attr("data-loading-text"));
}


function CloseButton(control) {
    $(control).removeClass('running ld ld-heartbeat loading');
    $(control).children("span").text($(control).attr("data-text"));
}


function ShowUserPanel(text1, text2, type, showspinner) {

    var alert = "";

    switch (type) {
        case "INFO":
            alert = alertinfo;
            break;
        case "ERROR":
            alert = dangerinfo;
            break;
        case "WARNING":
            alert = warninginfo;
            break;
        case "SUCESS":
            alert = successinfo;
            break;
        default:
            alert = alertinfo;
            break;
    }
    alert = alert.replace("{0}", text1);
    alert = alert.replace("{1}", text2);
    if (showspinner) {
        alert = alert.replace("{2}", spinner);
    }
    else {
        alert = alert.replace("{2}", "");
    }
    return alert;
}



function validarEmail(valor) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/.test(valor)) {
        return true;
    } else {
        return false;
    }
}


function validate(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}