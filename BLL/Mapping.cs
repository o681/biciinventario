﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using DTO;

namespace BLL
{
    public static class Mapping
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg => {
                // This line ensures that internal properties are also mapped over.
                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UsuarioDTO, Usuario>();
            CreateMap<MaterialDTO, Material>();
            CreateMap<List<MaterialDTO>, List<Material>>();
            CreateMap<Usuario, UsuarioDTO>().ForMember(act => act.NombreRol, dest => dest.Ignore());
            CreateMap<List<UsuarioDTO>, List<Usuario>>();
            CreateMap<TipoMovimientoDTO, TipoMovimiento>();
            CreateMap<RolDTO, Rol>();

        }
    }
}
