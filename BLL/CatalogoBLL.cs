﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Models;
using DTO;

namespace BLL
{
    public class CatalogoBLL
    {
        public List<TipoMovimientoDTO> CatalogoTipos()
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    List<TipoMovimiento> tipos = dbContext.TipoMovimiento.ToList();
                    if (tipos == null || tipos.Count <= 0)
                        return null;
                    var tiposDTO = Mapping.Mapper.Map<List<TipoMovimientoDTO>>(tipos);
                    return tiposDTO;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<RolDTO> CatalogoRoles()
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    List<Rol> tipos = dbContext.Rol.ToList();
                    if (tipos == null || tipos.Count <= 0)
                        return null;
                    var rolesDTO = Mapping.Mapper.Map<List<RolDTO>>(tipos);
                    return rolesDTO;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
