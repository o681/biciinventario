﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using Models;
using System.Configuration;

namespace BLL
{
    public class InventarioBLL
    {

        /// <summary>
        /// Retorna el historial de movimientos de un material
        /// </summary>
        /// <param name="InventarioID"></param>
        /// <returns></returns>
        public List<InventarioTransaccionDTO> SelectHistorial(int InventarioID)
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var Inventario = (from it in dbContext.InventarioTransaccion
                                      join i in dbContext.Inventario on it.InventarioID equals i.InventarioID
                                      join m in dbContext.Material on i.MaterialID equals m.MaterialID
                                      join u in dbContext.Usuario on it.UsuarioAuditID equals u.UsuarioID
                                      join t in dbContext.TipoMovimiento on it.TipoMovimientoID equals t.TipoMovimientoID
                                      where it.InventarioID == InventarioID
                                      select new InventarioTransaccionDTO
                                      {
                                          FechaInsercion = it.FechaInsercion,
                                          Motivo = it.Motivo,
                                          NombreMaterial = m.Nombre,
                                          TipoMovimiento = t.Nombre,
                                          Cantidad = it.Cantidad,
                                          Usuario = u.Nombres + " " + u.Apellidos
                                      }).ToList();

                    return Inventario;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Retorna el historial de movimientos de todos los materiales
        /// </summary>
        /// <returns></returns>
        public List<InventarioTransaccionDTO> TodoHistorial()
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var Inventario = (from it in dbContext.InventarioTransaccion
                                      join i in dbContext.Inventario on it.InventarioID equals i.InventarioID
                                      join m in dbContext.Material on i.MaterialID equals m.MaterialID
                                      join u in dbContext.Usuario on it.UsuarioAuditID equals u.UsuarioID
                                      join t in dbContext.TipoMovimiento on it.TipoMovimientoID equals t.TipoMovimientoID
                                      select new InventarioTransaccionDTO
                                      {
                                          FechaInsercion = it.FechaInsercion,
                                          Motivo = it.Motivo,
                                          NombreMaterial = m.Nombre,
                                          TipoMovimiento = t.Nombre,
                                          Cantidad = it.Cantidad,
                                          Usuario = u.Nombres + " " + u.Apellidos
                                      }).ToList();

                    return Inventario;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// Retorna todo el inventario almacenado en el sistema.
        /// </summary>
        /// <returns></returns>
        public List<InventarioDTO> TodoInventario()
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var Inventario = (from p in dbContext.Material
                                      join e in dbContext.Inventario
                                on p.MaterialID equals e.MaterialID
                                      select new InventarioDTO
                                      {
                                          Cantidad = e.Cantidad,
                                          InventarioID = e.InventarioID,
                                          NombreMaterial = p.Nombre
                                      }).ToList();

                    return Inventario;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Retorna el inventario de un material dado
        /// </summary>
        /// <param name="MaterialID"></param>
        /// <returns></returns>
        public InventarioDTO InventarioMaterial(int MaterialID)
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var Inventario = (from p in dbContext.Material
                                      join e in dbContext.Inventario
                                      on p.MaterialID equals e.MaterialID
                                      where p.MaterialID == MaterialID
                                      select new InventarioDTO
                                      {
                                          Cantidad = e.Cantidad,
                                          InventarioID = e.InventarioID,
                                          NombreMaterial = p.Nombre,
                                          MaterialID = p.MaterialID
                                      }).FirstOrDefault();

                    return Inventario;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Retorna el inventario de un material dado
        /// </summary>
        /// <param name="MaterialID"></param>
        /// <returns></returns>
        public InventarioDTO InventarioByID(int InventarioID)
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var Inventario = (from p in dbContext.Material
                                      join e in dbContext.Inventario
                                      on p.MaterialID equals e.MaterialID
                                      where e.InventarioID == InventarioID
                                      select new InventarioDTO
                                      {
                                          Cantidad = e.Cantidad,
                                          InventarioID = e.InventarioID,
                                          NombreMaterial = p.Nombre,
                                          MaterialID = p.MaterialID
                                      }).FirstOrDefault();

                    return Inventario;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Realiza un movimiento de inventario de un material dado.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="UsuarioID"></param>
        /// <returns></returns>
        public Response RealizarMovimiento(MovimientoDTO data, int UsuarioID)
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var inventacioActual = dbContext.Inventario.FirstOrDefault(x => x.InventarioID == data.InventarioID);
                    if (inventacioActual == null)
                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, inventario no encontrado." };

                    switch (data.TipoMovimientoID)
                    {
                        //Ingreso
                        case 1:
                            inventacioActual.Cantidad = inventacioActual.Cantidad + data.CantidadMovimiento;
                            dbContext.InventarioTransaccion.Add(new InventarioTransaccion()
                            {
                                Cantidad = data.CantidadMovimiento,
                                InventarioID = data.InventarioID,
                                FechaInsercion = DateTime.Now,
                                TipoMovimientoID = data.TipoMovimientoID,
                                UsuarioAuditID = UsuarioID,
                                Motivo = data.Motivo
                            });

                            if (dbContext.SaveChanges() > 0)
                                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Movimiento de material realizado exitosamente" };

                            return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, no es posible realizar el movimiento en el inventario." };
                        //Salida
                        case 2:
                            if (inventacioActual.Cantidad < data.CantidadMovimiento)
                                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, no tiene la cantidad suficiente de material." };

                            inventacioActual.Cantidad = inventacioActual.Cantidad - data.CantidadMovimiento;
                            dbContext.InventarioTransaccion.Add(new InventarioTransaccion()
                            {
                                Cantidad = data.CantidadMovimiento,
                                InventarioID = data.InventarioID,
                                FechaInsercion = DateTime.Now,
                                TipoMovimientoID = data.TipoMovimientoID,
                                UsuarioAuditID = UsuarioID,
                                Motivo = data.Motivo
                            });

                            if (dbContext.SaveChanges() > 0)
                            {
                                //Valida si tiene que enviar correo por cantidades bajas del material
                                int CantidadMinima = Convert.ToInt16(ConfigurationManager.AppSettings["cantidadminima"]);
                                if (inventacioActual.Cantidad <= CantidadMinima)
                                {
                                    EnviarCorreo(data);
                                }

                                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Movimiento de material realizado exitosamente" };
                            }

                            return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, no es posible realizar el movimiento en el inventario." };
                    }

                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, no es posible realizar el movimiento en el inventario." };

                }
            }
            catch (Exception ex)
            {
                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = ex.Message };
            }
        }


        /// <summary>
        /// Envia un correo electronico a todos los administradores cuando la cantidad de material
        /// disponible esta igual o por debajo del umbral.
        /// </summary>
        /// <param name="data"></param>
        public void EnviarCorreo(MovimientoDTO data)
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var administradores = dbContext.Usuario.Where(x => x.RolId == 1).ToList();
                    if(administradores != null && administradores.Count > 0)
                    {
                        List<string> correos = new List<string>();
                        foreach (Usuario item in administradores)
                        {
                            correos.Add(item.Correo);
                        }
                        //Saca la información del material
                        var info = dbContext.Inventario.FirstOrDefault(x => x.InventarioID == data.InventarioID);
                        var material = dbContext.Material.FirstOrDefault(x => x.MaterialID == info.MaterialID);
                        CorreoBLL.Email(correos, "Pocas unidades material",string.Format("Tiene pocas unidades del material {0}, codigo {1}, unidades disponibles {2}",material.Nombre,material.Codigo,info.Cantidad));
                    }
                }         
            }
            catch (Exception ex)
            {

            }
        }
    }
}
