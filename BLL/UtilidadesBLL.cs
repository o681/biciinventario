﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UtilidadesBLL
    {

        public static bool EsCorreoValido(string source)
        {
            return new EmailAddressAttribute().IsValid(source);
        }

        public enum EstadoAplicacion
        {

            Success = 200,
            SinSesion = 400,
            ServerError = 500
        }

        public static byte[] GenerarPassword(string userPassword, out string llave)
        {
            string salt = RandomString(10);
            llave = salt;
            byte[] bytes_contraseña = Encoding.UTF8.GetBytes(userPassword);
            byte[] bytes_salt = Encoding.UTF8.GetBytes(salt);
            return GenerateSaltedHash(bytes_contraseña, bytes_salt);
        }

        public static byte[] ValidaPassword(string userPassword, string llave)
        {
            byte[] bytes_contraseña = Encoding.UTF8.GetBytes(userPassword);
            byte[] bytes_salt = Encoding.UTF8.GetBytes(llave);
            return GenerateSaltedHash(bytes_contraseña, bytes_salt);
        }

        private static string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        private static byte[] GenerateSaltedHash(byte[] plainText, byte[] salt)
        {
            using (SHA256 algorithm = SHA256.Create())
            {

                byte[] plainTextWithSaltBytes =
                  new byte[plainText.Length + salt.Length];

                for (int i = 0; i < plainText.Length; i++)
                {
                    plainTextWithSaltBytes[i] = plainText[i];
                }
                for (int i = 0; i < salt.Length; i++)
                {
                    plainTextWithSaltBytes[plainText.Length + i] = salt[i];
                }

                return algorithm.ComputeHash(plainTextWithSaltBytes);
            }
        }
    }
}
