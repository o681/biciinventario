﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using DAL;

namespace BLL
{
    public class MaterialBLL
    {

        /// <summary>
        /// Elimina un material dado.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Response Eliminar(MaterialDTO data, int UsuarioID)
        {
            try
            {
                if (data == null)
                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Indique la información del material" };


                using (InventarioDbContext dbContext = new InventarioDbContext())
                {

                    InventarioDTO invMaterial = new InventarioBLL().InventarioMaterial(data.MaterialID);
                    if (invMaterial == null || invMaterial.Cantidad <= 0)
                    {
                        dbContext.Inventario.Remove(dbContext.Inventario.Single(x => x.MaterialID == data.MaterialID));
                        dbContext.Material.Remove(dbContext.Material.Single(x => x.MaterialID == data.MaterialID));
                        if (dbContext.SaveChanges() > 0)
                            return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Material elimininado exitosamente" };

                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, el material no se puede eliminar." };
                    }
                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, el material no se puede eliminar, porque tiene inventario." };

                }
            }
            catch (Exception ex)
            {
                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = ex.Message };
            }
        }


        public Response Editar(MaterialDTO data, int UsuarioID)
        {
            try
            {
                if (data == null)
                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Indique la información del material" };


                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var material = dbContext.Material.SingleOrDefault(b => b.MaterialID == data.MaterialID);

                    if (material == null)
                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, no encontramos la información del material." };

                    material.Nombre = data.Nombre;
                    material.Codigo = data.Codigo;
                    if(dbContext.SaveChanges() > 0)
                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Material actualizado exitosamente" };

                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, el material no se puede actualizar." };
                }
            }
            catch (Exception ex)
            {
                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = ex.Message };
            }
        }

        public Response Registrar(MaterialDTO data, int UsuarioID)
        {
            try
            {
                Material existe = Existe(data.Codigo);
                if (existe != null)
                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Este material ya se encuentra registrado." };

                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    Material newMaterial = new Material();
                    newMaterial.Activo = true;
                    newMaterial.Codigo = data.Codigo;
                    newMaterial.Nombre = data.Nombre;
                    newMaterial.FechaInsercion = DateTime.Now;
                    newMaterial.UsuarioAuditID = UsuarioID;
                    dbContext.Material.Add(newMaterial);

                    if (dbContext.SaveChanges() > 0)
                    {
                        //Registra el inventario
                        dbContext.Inventario.Add(new Inventario()
                        {
                            Activo = true,
                            FechaInsercion = DateTime.Now,
                            Cantidad = 0,
                            MaterialID = newMaterial.MaterialID
                        });
                        dbContext.SaveChanges();
                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Material registrado exitosamente" };
                    }

                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, el material no se puede registrar" };

                }
            }
            catch (Exception ex)
            {
                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = ex.Message };
            }
        }

        public Material Existe(string Codigo)
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    return dbContext.Material.FirstOrDefault(x => x.Codigo.ToUpper() == Codigo.ToUpper());
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public MaterialDTO SelectById(int Id){
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    Material material = dbContext.Material.FirstOrDefault(x => x.MaterialID == Id);
                    var materialDTO = Mapping.Mapper.Map<MaterialDTO>(material);
                    return materialDTO;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<MaterialDTO> Todos()
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    List<Material> materiales = dbContext.Material.ToList();
                    if (materiales == null || materiales.Count <= 0)
                        return null;
                    var materialesDTO = Mapping.Mapper.Map<List<MaterialDTO>>(materiales);
                    return materialesDTO;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }



    }
}