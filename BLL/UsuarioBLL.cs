﻿using AutoMapper;
using DAL;
using DTO;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UsuarioBLL
    {

        /// <summary>
        /// Registra un usuario en el sistema
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Response Registrar(RegistroDTO data)
        {
            try
            {
                Usuario newuser = new Usuario();
                string salt = string.Empty;
                newuser.Hash = Convert.ToBase64String(UtilidadesBLL.GenerarPassword(data.Password, out salt));
                newuser.Llave = salt;
                newuser.Correo = data.Correo;
                newuser.Nombres = data.Nombres;
                newuser.Apellidos = data.Apellidos;
                newuser.RolId = 2;
                newuser.UsuarioAuditID = 0;
                newuser.FechaInsercion = DateTime.Now;
                newuser.Activo = false;

                if (Existe(data.Correo) != null)
                {
                    Usuario user = Existe(data.Correo);
                    var userDTO = Mapping.Mapper.Map<UsuarioDTO>(user);
                    return new Response() { Code = 200, Message = "Usuario ya esta registrado", Items = userDTO };
                }

                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    dbContext.Usuario.Add(newuser);
                    if (dbContext.SaveChanges() > 0)
                    {
                        return new Response() { Code = 200, Message = "Se ha registrado exitosamente, para poder ingresar el administrador de la plataforma debe activarlo.", Items = newuser };
                    }

                    return new Response() { Code = 500, Message = "Error, el usuario no se pudo registrar.", };
                }

            }
            catch (Exception ex)
            {
                return new Response() { Code = 500, Message = ex.Message };
            }
        }

        /// <summary>
        /// Registra un usuario en la base de datos
        /// </summary>
        /// <param name="data"></param>
        /// <param name="UsuarioID"></param>
        /// <returns></returns>
        public Response Adicionar(UsuarioDTO data, int UsuarioID)
        {
            try
            {
                Usuario newuser = new Usuario();
                string salt = string.Empty;
                newuser.Hash = Convert.ToBase64String(UtilidadesBLL.GenerarPassword(data.Password, out salt));
                newuser.Llave = salt;
                newuser.Correo = data.Correo;
                newuser.Nombres = data.Nombres;
                newuser.Apellidos = data.Apellidos;
                newuser.RolId = data.RolId;
                newuser.UsuarioAuditID = 0;
                newuser.FechaInsercion = DateTime.Now;
                newuser.Activo = data.Activo;

                if (Existe(data.Correo) != null)
                {
                    Usuario user = Existe(data.Correo);
                    var userDTO = Mapping.Mapper.Map<UsuarioDTO>(user);
                    return new Response() { Code = 200, Message = "Este usuario ya esta registrado", Items = userDTO };
                }

                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    dbContext.Usuario.Add(newuser);
                    if (dbContext.SaveChanges() > 0)
                    {
                        return new Response() { Code = 200, Message = "Usuario registrado exitosamente", Items = newuser };
                    }

                    return new Response() { Code = 500, Message = "Error, el usuario no se pudo registrar.", };
                }

            }
            catch (Exception ex)
            {
                return new Response() { Code = 500, Message = ex.Message };
            }
        }

        /// <summary>
        /// Valida el login del usuario
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Response Login(LoginDTO data)
        {
            try
            {
                Usuario user = Existe(data.Correo);
                if (user == null)
                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "El usuario no esta registrado en el sistema" };

                string Hash = Convert.ToBase64String(UtilidadesBLL.ValidaPassword(data.Password, user.Llave));
                if (user.Hash != Hash)
                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Contraseña incorrecta." };

                if (!user.Activo)
                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Aun no estas activado para ingresar a la plataforma, comunicate con el administrador." };

                var userDTO = Mapping.Mapper.Map<UsuarioDTO>(user);
                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Usuario autenticado", Items = userDTO };
            }
            catch (Exception ex)
            {
                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = ex.Message };
            }
        }

        /// <summary>
        /// Valida la existencia de un usuario
        /// </summary>
        /// <param name="Correo"></param>
        /// <returns></returns>
        public Usuario Existe(string Correo)
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    return dbContext.Usuario.FirstOrDefault(x => x.Correo.ToUpper() == Correo.ToUpper());
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Retorna la informacion del usuario por el id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public UsuarioDTO SelectById(int Id)
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var usuario = (from u in dbContext.Usuario
                                   join r in dbContext.Rol
                                   on u.RolId equals r.RolId
                                   where u.UsuarioID == Id
                                   select new UsuarioDTO
                                   {
                                       NombreRol = r.Nombre,
                                       Apellidos = u.Apellidos,
                                       Correo = u.Correo,
                                       Nombres = u.Nombres,
                                       Telefono = u.Telefono,
                                       UsuarioID = u.UsuarioID
                                   }).FirstOrDefault();
                    return usuario;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Retorna todos los usuarios registrados en el sistema.
        /// </summary>
        /// <returns></returns>
        public List<UsuarioDTO> Todos()
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var Usuarios = (from u in dbContext.Usuario
                                    join r in dbContext.Rol on u.RolId equals r.RolId
                                    select new UsuarioDTO
                                    {
                                        Apellidos = u.Apellidos,
                                        NombreRol = r.Nombre,
                                        RolId = r.RolId,
                                        Correo = u.Correo,
                                        Activo = u.Activo,
                                        Nombres = u.Nombres,
                                        Telefono = u.Telefono,
                                        UsuarioID = u.UsuarioID
                                    }).ToList();
                    return Usuarios;
                }
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Activa o desactiva un usuario
        /// </summary>
        /// <param name="data"></param>
        /// <param name="UsuarioID"></param>
        /// <returns></returns>
        public Response ActivaDesactiva(UsuarioDTO data, int UsuarioID)
        {
            try
            {
                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var usuario = dbContext.Usuario.FirstOrDefault(x => x.UsuarioID == data.UsuarioID);
                    if (usuario == null)
                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, el usuario no existe" };

                    usuario.Activo = data.Activo;
                    if (dbContext.SaveChanges() > 0)
                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Cambio de estado del usuario realizado exitosamente" };

                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, no se pudo cambiar el estado al usuario." };
                }
            }
            catch (Exception ex)
            {
                return new Response() { Code = 500, Message = ex.Message };
            }
        }


        /// <summary>
        /// Elimina un usuario de la base de datos.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="UsuarioID"></param>
        /// <returns></returns>
        public Response Eliminar(UsuarioDTO data, int UsuarioID)
        {
            try
            {
                if (data == null)
                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Indique la información del usuario" };


                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    dbContext.Usuario.Remove(dbContext.Usuario.Single(x => x.UsuarioID == data.UsuarioID));
                    if (dbContext.SaveChanges() > 0)
                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Usuario eliminado exitosamente." };

                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, no se puede eliminar el usuario." };
                }
            }
            catch (Exception ex)
            {
                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = ex.Message };
            }
        }


        public Response Editar(UsuarioDTO data, int UsuarioID)
        {
            try
            {
                if (data == null)
                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Indique la información del usuario" };


                using (InventarioDbContext dbContext = new InventarioDbContext())
                {
                    var usuario = dbContext.Usuario.First(x => x.UsuarioID == data.UsuarioID);
                    if (usuario == null)
                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, el usuario no existe." };

                    usuario.Nombres = data.Nombres;
                    usuario.Apellidos = data.Apellidos;
                    usuario.Telefono = data.Telefono;
                    usuario.Correo = data.Correo;

                    if (dbContext.SaveChanges() > 0)
                        return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.Success, Message = "Usuario editado exitosamente." };

                    return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = "Error, no se puede editar el usuario." };
                }
            }
            catch (Exception ex)
            {
                return new Response() { Code = (int)UtilidadesBLL.EstadoAplicacion.ServerError, Message = ex.Message };
            }
        }



    }
}
